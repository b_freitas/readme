# README

Hi, I am Bruno Freitas and the purpose of this section is to tell you more about myself and what I value.

**Who I am**

I am a support engineer (EMEA) at Gitlab. Previously I worked as Linux Systems Administrator and as a Software Developer. I am based in Luanda, Angola. I have thing for automation so I am the perfect person to automate boring tasks, lol.

**What I Like**

When I am not working, I like to travel, play video games, research tech related stuff and I am also a cryptocurrency miner/enthusiast. Watching great movies is another passion of mine.

**What I value**


- I value transparency

- I value experiences

- I value knowledge

- I value growth

If you would like to know more about me, please leave your comments and help me maintain this page.

Cheers,